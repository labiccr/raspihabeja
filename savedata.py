from temperatura import Temperature
from serialarduino import callArduino
import requests
import os.path as path
import socket
import json
from datetime import datetime
import sys

hotelName = socket.gethostname()
url = 'https://hoteldeabejas-619ca.firebaseio.com/hoteles/names/'

class sendData:
    def fullSensor():
        urlFinal = url + hotelName.replace('.','-', -1) + '.json?print=silent'
        hpt = Temperature.sensors()
        arduino = callArduino.sendArduino()
        info = info_data(hpt[0],hpt[1],hpt[2],arduino[0],arduino[1],arduino[2],datetime.now())
        x = requests.post(urlFinal, json.dumps(info.__dict__))
        print (x)



class info_data:
    def __init__(self,humidity,pressure,temperature,co2,pm25,uv,dt):
        self.humidity = humidity
        self.pressure = pressure
        self.temperature = temperature
        self.co2 = co2
        self.pm25 = pm25
        self.uv = uv
        self.date = dt.strftime('%d/%m/%Y')
        self.hour = dt.strftime('%H:%M:%S')

class info_data2:
    def __init__(self,errors,dt):
        self.error = errors
        self.date = dt.strftime('%d/%m/%Y')
        self.hour = dt.strftime('%H:%M:%S')
        

    #def sendImage():
try:
    x = sendData
    x.fullSensor()
except:
    url2 = 'https://hoteldeabejas-619ca.firebaseio.com/errores/hoteles/'
    urlFinal = url2 + hotelName.replace('.','-', -1) + '.json?print=silent'
    info = info_data2(str(sys.exc_info()[0]),datetime.now())
    x = requests.post(urlFinal, json.dumps(info.__dict__))
    print ('error :( ')
    

