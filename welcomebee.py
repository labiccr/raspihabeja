#!/usr/bin/env python3
import RPi.GPIO as GPIO
import time
import picamera
from time import sleep
from datetime import datetime
import subprocess
import os
import shutil
import socket
import sys
import requests
import json

laser_1_input = 17
laser_2_input = 27


class info_data:
    def __init__(self, errors, dt):
        print('se inicio deteccion de laser')
        self.error = errors
        self.date = dt.strftime('%d/%m/%Y')
        self.hour = dt.strftime('%H:%M:%S')


def photo_test():
    with picamera.PiCamera() as camera:
        #camera.vflip = True
        #camera.hflip = True
        directory = datetime.now().strftime('%Y%m%d')
        os.chdir("/home/pi/Documents/raspihabeja" )
        parent_dir =  os.getcwd()
        path = os.path.join(parent_dir, directory)
        if(not os.path.isdir(path)):
            os.mkdir(path)
        camera.resolution = (1920, 1080)
        camera.annotate_text = 'airbeenbee.io - ' + \
            datetime.now().strftime('%Y-%m-%d')
        camera.capture("./" + datetime.now().strftime('%Y%m%d') +
                       "/beecamera" + str(datetime.now())+'.jpg', resize=(640, 420))


def laser_a_rising_event(channel):
    time.sleep(.2)
    if GPIO.input(laser_1_input) or GPIO.input(laser_2_input):
        print("digan wiskas")
        try:
            with picamera.PiCamera() as camera:
                #camera.vflip = True
                #camera.hflip = True
                directory = datetime.now().strftime('%Y%m%d')
                os.chdir("/home/pi/Documents/raspihabeja" )
                parent_dir =  os.getcwd()
                path = os.path.join(parent_dir, directory)
                if(not os.path.isdir(path)):
                    os.mkdir(path)
                camera.resolution = (1920, 1080)
                camera.annotate_text = 'airbeenbee.io - ' + \
                    datetime.now().strftime('%Y-%m-%d')
                camera.capture("./" + datetime.now().strftime('%Y%m%d') +
                               "/beecamera" + str(datetime.now())+'.jpg', resize=(640, 420))
                # guarda en usb
                # dir = os.listdir(path='/media/pi/')
                # if len(dir) != 0:
                #       usb = '/media/pi/'+dir[0]+'/beecamera'+ str(time.time()) + '.jpg'
                #       photo = '/home/pi/hotel/beecamera.jpg'
                #       with open(photo, 'rb') as forigen:
                #             with open(usb, 'wb') as fdestino:
                #                   shutil.copyfileobj(forigen, fdestino)
                #                   print('transferencia exitosa a '+ dir[0])
                #       time.sleep(1)
        except:
            url = 'https://hoteldeabejas-619ca.firebaseio.com/errores/hoteles/'
            hotelName = socket.gethostname()
            urlFinal = url + \
                hotelName.replace('.', '-', -1) + '.json?print=silent'
            info = info_data(str(sys.exc_info()[0]), datetime.now())
            requests.post(urlFinal, json.dumps(info.__dict__))
            #subprocess.call('sshpass -p "22051990" scp /home/pi/hotel/beecamera.jpg CesarFlores@10.70.1.81:Downloads', shell=True)
    return


print('configuracion rbpi')
GPIO.setmode(GPIO.BCM)
GPIO.setup(laser_1_input, GPIO.IN)
GPIO.remove_event_detect(laser_1_input)
GPIO.add_event_detect(laser_1_input, GPIO.RISING,
                      callback=laser_a_rising_event)

GPIO.setmode(GPIO.BCM)
GPIO.setup(laser_2_input, GPIO.IN)
GPIO.remove_event_detect(laser_2_input)
GPIO.add_event_detect(laser_2_input, GPIO.RISING,
                      callback=laser_a_rising_event)


while True:
    # photo_test()
    time.sleep(.5)
 # wait so we don't lock up proccessor .1
try:
    time.sleep(1)
except:
    GPIO.cleanup()  # cleanup
    exit()
