# raspiHAbeja

Sistema centralizado para el control del hotel de abeja, medira sensores como los
mg811 (co2)
laser 
MQ7 (sensor de gas)
BME280 (temperatura, humedad, presion)
camaras (raspi o webcam)
DSM501 (particulas)

instalar 
sudo apt update
sudo apt install rpi.gpio
sudo apt install python3-picamera
sudo apt install python3-pip
sudo apt install python-pip
pip3 install smbus2
pip3 install pyserial
sudo apt install ffmpeg
sudo apt install fswebcam
pip install requests
sudo apt install python-setuptools
pip install Adafruit-GPIO
git clone https://github.com/adafruit/Adafruit_Python_MCP3008.git
cd Adafruit_Python_MCP3008
sudo python3 setup.py install