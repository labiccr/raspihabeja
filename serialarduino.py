import serial, time

class callArduino:
    def sendArduino():
        try:
            arduino = serial.Serial('/dev/ttyUSB0', 9600, timeout=45)
            flagCharacter = 'w'
            time.sleep(2)
            arduino.write(flagCharacter.encode())
            rawString = arduino.readline()
            arduino.close()
            slist = rawString.decode("utf-8").split(',',-1)
            if(len(slist) == 0):
                slist = ['0','0','0']

            return slist

        except:
            try:
                arduino = serial.Serial('/dev/ttyACM0', 9600, timeout=45)
                flagCharacter = 'w'
                time.sleep(2)
                arduino.write(flagCharacter.encode())
                rawString = arduino.readline()
                arduino.close()
                slist = rawString.decode("utf-8").split(',',-1)
                if(len(slist) == 0):
                    slist = ['0','0','0']

                return slist
            except :
                return ['0','0','0']
            
        

# x = callArduino
# print(x.sendArduino())


